from flask import Flask, render_template, redirect, url_for, request
from coreapp import app
from datetime import date

@app.route('/' ,methods=['GET','POST'])
def index():
    month = list()
    for i in range(1,13):
        month.append(i)
    day = list()
    for i in range(1,32):
        day.append(i)
    year = list()
    for i in range(2014,2021):
        year.append(i)
    if request.method == 'GET':
        return render_template("index.html", title='Enter Date', month = month, day = day, year = year)
    if request.method == 'POST':
        return redirect(url_for('dow',month = request.form['month'], day = request.form['day'], year = request.form['year']))

@app.route('/dow')
def dow():
    try:
        month = request.args.get('month', None)
        day = request.args.get('day', None)
        year = request.args.get('year', None)
        m = int(month)
        d = int(day)
        y = int(year)
        ymd = date(y, m, d)
        day = ymd.strftime("%A")
        answer = day
    except Exception as e:
        answer = str(e)    
    return render_template("day.html", title='Day of Week', answer = answer)